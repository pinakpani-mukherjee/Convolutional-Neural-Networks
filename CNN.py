#Convulutional Neural Networks

#importing libraries for Neural networks
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten

#initializing the CNN
classifier = Sequential()

#adding layers
#1st Convolutional Layer
classifier.add(Convolution2D(32,(3,3),input_shape = (64,64,3),activation = "relu"))

#2nd Pooling Layer
classifier.add(MaxPooling2D(pool_size = (2,2)))

#flattening the data
classifier.add(Flatten())

#adding the ANN to the Pooled Data
classifier.add(Dense(128,activation = "relu"))  

#final output layer
classifier.add(Dense(1,activation = "sigmoid"))

#compiling the ANN
classifier.compile(optimizer = "adam",loss ="binary_crossentropy", metrics = ['accuracy'])

#fitting images to CNN by preprocessing, code taken form Keras(Data Preprocessing)
from keras.preprocessing.image import ImageDataGenerator
train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

training_set = train_datagen.flow_from_directory('dataset/training_set',
                                                target_size=(64, 64),
                                                batch_size=32,
                                                class_mode='binary')

test_set = test_datagen.flow_from_directory('dataset/test_set',
                                            target_size=(64, 64),
                                            batch_size=32,
                                            class_mode='binary')

#training the neural network
classifier.fit_generator(
        training_set,
        steps_per_epoch=8000,
        epochs=25,
        validation_data=test_set,
        validation_steps=2000)
